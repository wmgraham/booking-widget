function ready(fn) {
	if (document.attachEvent ? document.readyState === "complete" : document.readyState !== "loading"){
		fn();
	} else {
		document.addEventListener('DOMContentLoaded', fn);
	}
}

function addEvent(elem, event, fn) {
	if(elem != null) {
		if (elem.addEventListener) {
			elem.addEventListener(event, fn, false);
		} else {
			elem.attachEvent("on" + event, function() {
				// set this pointer same as addEventListener when fn is called
				return(fn.call(elem, window.event));
			});
		}
	}
}

addEvent(document.getElementById('form1'), 'submit', function() {
	parent.postMessage('submit', '*');
});
